import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../firebase.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {
  exampleForm: FormGroup;
  avatarLink: string = "https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg";


  constructor( private fb: FormBuilder, private firebaseService: FirebaseService) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.exampleForm = this.fb.group({
      area: [''],
      copa: [''],
      descripcion: [''],
      dia: [''],
      mes: [''],
      hora: [''],
      distrito: [''],
      fechaCierre: [''],
      formulario: [''],
      id: [''],
      image: [''],
      link: [''],
      lugar: [''],
      organizado: [''],
      primero: [''],
      segundo: [''],
      tercero: [''],
      sorteos: ['']
    });
  }

  resetFields(){
    this.avatarLink = "https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg";
    this.exampleForm = this.fb.group({
      area: [''],
      copa: [''],
      descripcion: [''],
      dia: [''],
      mes: [''],
      hora: [''],
      distrito: [''],
      fechaCierre: [''],
      formulario: [''],
      id: [''],
      image: [''],
      link: [''],
      lugar: [''],
      organizado: [''],
      primero: [''],
      segundo: [''],
      tercero: [''],
      sorteos: ['']
    });
  }

  onSubmit(value){
    this.firebaseService.createUser(value, this.avatarLink)
    .then(
      res => {
        this.resetFields();
      }
    )
  }

}
