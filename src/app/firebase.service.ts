import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';



@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

 
  constructor(public db: AngularFirestore) { }

  createUser(value, avatar){
    return this.db.collection('events').add({
      area: value.area,
      copa: value.copa,
      descripcion: value.descripcion,
      dia: value.dia,
      mes: value.mes,
      hora: value.hora,
      distrito: value.distrito,
      fechacierre: value.fechaCierre,
      formulario: value.formulario,
      id: value.id,
      image: value.image,
      link: value.link,
      lugar: value.lugar,
      organizado: value.organizado,
      primero: value.primero,
      segundo: value.segundo,
      tercero: value.tercero,
      sorteos: value.sorteos
    });
  }
}
